from math import radians, cos, sin, asin, sqrt
import os
import heapq
import geocoder
import googlemaps

baselocation = "SAP, Bangalore"


def sv(vendors):
	filelist = os.listdir("service1")
	list_of_lists = []

	def getdistance(sourceaddr, destinationaddr):
		gmaps = googlemaps.Client(key='AIzaSyA07_hGSjBW3axskRgOCh3Ee5ffZO4kTuI')

		matrix = gmaps.distance_matrix(sourceaddr, destinationaddr,mode="driving")
		return float(matrix['rows'][0]['elements'][0]['distance']['value'])/1000

	for i in range(len(filelist)):
		list_of_lists.append([])
		f = open("service1/"+filelist[i],"r")
		for line in f:
			list_of_lists[i].append(line.split(":")[1].strip())	
		
	#scoring formula - price vs quality.
	#lets give quality a score of 2.
	# for scoring over the price value of a service or a product, we take the highest price as a base value and reduce it to a fraction lower than 10 and
	# the same reducing factor is used to reduce the current value as well.
	# the differences in price between the reduced base value and reduced current values multiplied by a factor of 1 gives the price scoring factor.
	#a similar scoring feature is applied for distance.

	distlist = []
	for i in list_of_lists:
		dist = getdistance(baselocation,i[1])
		# print "dist b/w SAP Bangalore and " + i[1] + " is " + str(dist)
		distlist.append(dist)

	basedist = float(max(distlist))
	distreducer = 10**(len(str(abs(int(basedist))))-1)

	baseprice = float(max(i[2] for i in list_of_lists))
	pricereducer = 10**(len(str(abs(int(baseprice))))-1 )

	#scores
	scores = []

	for i in list_of_lists:
		scores.append((basedist/distreducer-distlist[list_of_lists.index(i)]/distreducer)+(baseprice/pricereducer-float(i[2])/pricereducer)+float(i[3])*2)

	# print scores

	print "best vendor is "
	print list_of_lists[scores.index(max(scores))][0]
	print "\n" 	
	print "next best choice is "
	print list_of_lists[scores.index(heapq.nlargest(2, scores)[1])][0]


