# Reference :   https://stackoverflow.com/questions/372885/how-do-i-connect-to-a-mysql-database-in-python
#               http://www.mikusa.com/python-mysql-docs/index.html

import MySQLdb

db = MySQLdb.connect(host="localhost",      # your host, usually localhost
                     user="root",           # your username
                     passwd="",             # your password
                     db="evibot")           # name of the data base

# you must create a Cursor object. It will let
#  you execute all the queries you need
cur = db.cursor()

# Use all the SQL you like
cur.execute("SELECT * FROM YOUR_TABLE_NAME")

# print all the first cell of all the rows
for row in cur.fetchall():
    print row[0]

db.close()
