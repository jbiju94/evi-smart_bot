import apiai


class AIHandler:
    CLIENT_ACCESS_TOKEN = '862ff10b28594aef8f9a57dbfb9e3329'

    def __init__(self, text):
        self.text = text
        self.ai = apiai.ApiAI(AIHandler.CLIENT_ACCESS_TOKEN)
        self.request = ""

    """
    # Get intent for text from API.AI
    # @param {none}
    """
    def get_intent(self):
        self.request = self.ai.text_request()
        self.request.lang = 'en'  # optional, default value equal 'en'
        self.request.session_id = "123"  # same as request id

        self.request.query = self.text

        self.request = self.request.getresponse()

        return self.request.read()
