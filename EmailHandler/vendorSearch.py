'''
vendorSearch.py:

Python script to scrape vendors using scrapy from sites like justdial etc.


'''
from scrapy.selector import Selector 
from scrapy.http import HtmlResponse
import re
from requests import get,post,delete
import xml.etree.ElementTree as ET
from lxml import etree


'''
scrapeText():

function to scrape text data from given message.right now it is for text.
for links work in progress

the following data is available from the list.
	1. rating given by justdial
	2. Address
	3. Phone Number


'''
def getNewVendors(city, item):
	body=''' '''
	headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'}
	print 'https://www.justdial.com/'+city+'/'+item
	response = get('https://www.justdial.com/'+city+'/'+item,headers = headers) 
	txt = response.text.encode('utf-8')
	# parser = ET.XMLParser(encoding="utf-8")
	# tree = ET.fromstring(response.text, parser=parser)
	# xml_response = ET.fromstring(txt)
	parser = etree.XMLParser(recover=True)
	tree = etree.fromstring(txt, parser=parser)
	body = etree.tostring(tree)
	# print(etree.dump(tree))
	# print type(tree)
	# print ''.join(tree.xpath("//span/text()")).encode('utf-8')
	#print txt
	vendor_data = Selector(text = txt).xpath('//span/a/text()').extract()
	
	# vendor_data = ','.join(vendor_data).encode('utf-8')
	vendor_list = [[]]
	count = 0
	for i in range(len(vendor_data)):
		if '+' in vendor_data[i]:
			# print vendor_data[i]
			vendor_list.extend(list([vendor_data[i-1],vendor_data[i]]))


	print vendor_list
	return vendor_list
if __name__ == '__main__':
	getNewVendors("Bangalore","Trophy")