from flask import Flask
from flask import request
from flask import jsonify
from email_handler import EmailHandler

import json

import requests

app = Flask(__name__)


@app.route('/newMail', methods=['POST'])
def get_mail():
    POST_URL = "http://localhost:5002/newEvent"
    email_id = request.form['emailId']
    eH = EmailHandler()
    response = json.dumps(eH.get_mail(email_id))
    r = requests.post(POST_URL, data={'email': response})
    return jsonify(eH.get_mail(email_id))


@app.route('/sendMail', methods=['POST'])
def send_mail():
    __to = request.form['to']
    __sender = request.form['from']
    __subject = request.form['subject']
    __body = request.form['body']
    __attachment = request.form['attachment']
    eH = EmailHandler()
    eH.send_mail1(__sender,__to,__subject,__body,__attachment)
    return "hello"

if __name__ == '__main__':
    app.run(port=5001)
