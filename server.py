from flask import Flask
from flask import request
from flask import jsonify
import json
import requests

from email_handler import EmailHandler
from logic_handler import LogicHandler


app = Flask(__name__)


@app.route('/newMail', methods=['POST'])
def get_mail():
    email_id = request.form['emailId']
    eH = EmailHandler()
    response = eH.get_mail(email_id)

    lg = LogicHandler(json.dumps(response))
    resp = lg.trigger_work_flow()

    return resp


@app.route('/sendMail', methods=['POST'])
def send_mail():
    print "got response:",request.form['subject']
    #if request.method == 'POST':
    __to = request.form['to']
    __sender = request.form['from']
    __subject = request.form['subject']
    __body = request.form['body']
    __attachment = request.form['attachment']
    eH = EmailHandler()
    eH.send_mail1(__sender,__to,__subject,__body,__attachment)
    return "hello"

if __name__ == '__main__':
    app.run(port=5001)
