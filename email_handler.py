# -*- coding: utf-8 -*-
import imaplib
import email
import base64
import re
import json
# Sending Emails
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from templateGenerator import genPromt as promt


class EmailHandler:
    __USER = 'concur.api.test@gmail.com'
    __PASSWORD = 'welcome@123'

    POST_URL = "http://localhost:5001/newMail"

    def __init__(self):
        self.__subject = ""
        self.__to = ""
        self.__sender = ""
        self.__attachments = ""

    # ============  PRIVATE =================
    def __connect_to_mailbox(self):
        # connecting to the gmail imap server
        self.__mailbox = imaplib.IMAP4_SSL("gmail-imap.l.google.com")
        self.__mailbox.login(EmailHandler.__USER, EmailHandler.__PASSWORD)
        self.__mailbox.select("INBOX")  # here you a can choose a mail box like INBOX instead

    def __extract_info(self, msg):
        self.__connect_to_mailbox()
        mail_parts = sorted(list(msg.walk()), key=lambda a: a.get_content_type())
        img_attachment = [part.get_payload() for part in mail_parts if 'image' in part.get_content_type()]
        img = bytearray()
        img.extend(bytes(base64.b64decode(img_attachment[0])))
        msg_body = [part.get_payload() for part in mail_parts if
                    'text' in part.get_content_type() and 'plain' in part.get_content_subtype()]
        self.__body = ''.join(msg_body)

        msg_content = re.sub(r'[\W_]+', ',', self.__body)
        self.__content = ' '.join(msg_content.split(','))

        self.__subject = msg['subject']
        self.__to = msg['to']
        self.__sender = msg['from'].partition('<')[-1].rpartition('>')[0]
        self.__emailDump = {"From": self.__sender,
                            "To": self.__to,
                            "Subject": self.__subject,
                            "Body": self.__content,
                            "Attachment": self.__attachments
                            }

    # ================== PUBLIC ========================

    '''
    Get Email by Id
    params= int email ID
    '''
    def get_mail(self, email_id):
        self.__connect_to_mailbox()
        print email_id
        resp, data = self.__mailbox.fetch(1,'(RFC822)')
        # fetching whole mail
        mail_content = data[0][1]
        msg = email.message_from_string(mail_content.decode('utf-8'))# parsing the mail content to get a mail object
        self.__extract_info(msg)
        return self.__emailDump



    '''
    #Send Email by Id
    #@params {String} sender
    #@params {String} to
    #@params {String} subject
    #@params {String} content
    #@params {String} attachment
    '''
    @staticmethod
    def send_mail1(sender, to, subject, content, attachment, typeOf, request_id):
        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = sender
        msg['To'] = to

        if typeOf is 'PROMT_ASK':
            html = promt(content, request_id)
        else:
            # Create the body of the message (a plain-text and an HTML version).
            text = content
            html = """\
                <html>
                    <head></head>
                    <body>
                        <p>""" + content + """
                        </p>
                    </body>
                </html>
            """

        # Record the MIME types of both parts - text/plain and text/html.
        part1 = MIMEText(text, 'plain')
        part2 = MIMEText(html, 'html')

        # Attach parts into message container.
        # According to RFC 2046, the last part of a multipart message, in this case
        # the HTML message, is best and preferred.
        msg.attach(part1)
        msg.attach(part2)

        # Send the message via local SMTP server.
        s = smtplib.SMTP('smtp.gmail.com:587')
        # start TLS for security
        s.starttls()
        # Authentication
        s.login(EmailHandler.__USER, EmailHandler.__PASSWORD)
	#print s.verify(True)
        # sendmail function takes 3 arguments: sender's address, recipient's address
        # and message to send - here it is sent as one string.
        s.sendmail(sender, to, msg.as_string())
        s.quit()
