from flask import Flask
from flask import request
from flask import jsonify

from ai_handler import AIHandler

app = Flask(__name__)


# request for processing payload
@app.route('/getIntent', methods=['POST'])
def get_intent():
    __text = request.form['text']
    aiHand = AIHandler(__text)
    return aiHand.get_intent()


# Getting response for a specific payload
@app.route('/getResponse', methods=['POST'])
def get_response():
    __text = request.form['text']
    aiHand = AIHandler(__text)
    return "hello"


if __name__ == '__main__':
    app.run(port=5003)
