requests = dict()
vendors = dict()
items = list()
vendor_has = dict()

vendor = {'name': 'vendor1', 'contact': '9633017633', 'email': 'jbiju94@gmail.com', 'isRegistered': False}
vendor.update(vendor)
vendor = {'name': 'vendor2', 'contact': '9633017633', 'email': 'jbiju94@gmail.com', 'isRegistered': False}
vendor.update(vendor)
vendor = {'name': 'vendor3', 'contact': '9633017633', 'email': 'jbiju94@gmail.com', 'isRegistered': False}
vendor.update(vendor)

items.append("pen drive")
items.append("trophies")
items.append("bean bags")
items.append("pens")

vendor_item = {'name': 'pen drive', 'vendor': 'vendor1', 'price': 200}
vendor_has.update(vendor_item)
vendor_item = {'name': 'trophies', 'vendor': 'vendor1', 'price': 500}
vendor_has.update(vendor_item)
vendor_item = {'name': 'pens', 'vendor': 'vendor2', 'price': 100}
vendor_has.update(vendor_item)
vendor_item = {'name': 'bean bags', 'vendor': 'vendor3', 'price': 900}
vendor_has.update(vendor_item)
vendor_item = {'name': 'trophies', 'vendor': 'vendor3', 'price': 800}
vendor_has.update(vendor_item)


def add_new_request(data):
    requests.update(data)


def register_new_vendor(name):
    for _vendor in vendors:
        if _vendor[name] is name:
            _vendor['isRegistered'] = True
            name = _vendor['name']
            break

    return "Vendor " + name + "has been added."


def find_vendors_with_items(item_name):
    _vendors = list()
    for _vendor_item in vendor_has:
        if _vendor_item['name'] is item_name:
            _vendors.append(_vendor_item['vendor'])

    return _vendors


def is_vendor_registered(name):
    for _vendor in vendors:
        if _vendor[name] is name:
            if _vendor['isRegistered']:
                return True
    return False
