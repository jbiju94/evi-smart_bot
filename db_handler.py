import mysql.connector
from mysql.connector import Error
import uuid


class DBHandler:
    def __init__(self):
        self.conn = DBHandler.connect()

    @staticmethod
    def connect():
        """ Connect to MySQL database """
        try:
            DBHandler.conn = mysql.connector.connect(host='localhost',
                                                     database='evidb',
                                                     user='root',
                                                     password='')
            if DBHandler.conn.is_connected():
                print("Successfully Connected")
                return DBHandler.conn

        except Error as e:
            print(e)

        finally:
            DBHandler.conn.close()

    def add_request(self, data):

        query = "INSERT INTO request(request_id,user_id,vendor_id,item_name,quantity,request_status,deadline_date) " \
                "VALUES(%s,%s,%s,%s,%s,%s,%s)"
        args = (
            uuid.uuid4(), data['user'], data['vendor'], data['item'], data['qty'], data['status'],
            data['deadline_date'])

        cursor = self.conn.close()
        cursor.execute(query, args)

        if cursor.lastrowid:
            print('last insert id', cursor.lastrowid)

        else:
            print('last insert id not found')

        self.conn.commit()
        return cursor.lastrowid

    def is_existing_request(self, data):
        query = "SELECT * FROM request WHERE user_id = %s AND vendor_id = %s AND item_name = %s ORDER BY DESC issue_date"
        args = (data['user'], data['vendor'], data['item'])
        cursor = self.conn.close()
        cursor.execute(query, args)

        row = cursor.fetchone()

        if row is not None:
            return True
        else:
            return False

    def update_request_status(self, id, status):
        query = "UPDATE requests SET request_status = %s WHERE request_id = %s"
        args = (status, id)
        cursor = self.conn.close()
        cursor.execute(query, args)
