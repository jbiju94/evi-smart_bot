import requests
import json


class LogicHandler:

    API_SERVER_URL = "http://localhost:5003/"
    EMAIL_SERVER_URL = "http://localhost:5001/"

    def __init__(self, email):
        email = json.loads(email)
        self.__email = email
        self.__to = email['To']
        self.__from = email['From']
        self.__subject = email['Subject']
        self.__body = email['Body']

    """
    # Extract Required Data from response
    """
    def __extract_data(self, data):
        data = json.loads(data)
        if data['status']['code'] == 200:
            self.__source = data['result']['source']
            self.__source = 'domain'
            self.__query = data['result']['resolvedQuery']
            self.__action = data['result']['action']
            self.__response = data['result']['fulfillment']['speech']
            if self.__source is 'agent':
                self.__intent = data['result']['contexts']['name']
                self.__parameters = data['result']['contexts']['parameters']
            else:
                self.__intent = "GENERAL_CASE"
        else:
            self.__throw_error()


    """
    # request for intent for text
    """
    def __get_intent(self):
        r = requests.post(LogicHandler.API_SERVER_URL + "getIntent", data={'text': self.__body})
        print(r.status_code, r.reason)
        if r.status_code == 200:
            self.__extract_data(r.content)
            if self.__source is 'domain':
                print LogicHandler.EMAIL_SERVER_URL + "sendMail"
                r = requests.post(LogicHandler.EMAIL_SERVER_URL + "sendMail", data={'to': self.__from, 'from': self.__to, 'subject': self.__subject, 'body': self.__response, 'attachment': ''})
                print(r.status_code, r.reason)
        else:
            self.__throw_error()

        return self.__intent

    def __throw_error(self):
        return "Error"

    def trigger_work_flow(self):
        intent = self.__get_intent()
        return intent
