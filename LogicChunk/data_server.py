from flask import Flask
from flask import request
from flask import jsonify
import json
import uuid

from logic_handler import LogicHandler

app = Flask(__name__)

requests = dict()
vendors = dict()
items = list()
vendor_has = dict()

vendor = {'name': 'vendor1', 'contact': '9633017633', 'email': 'jbiju94@gmail.com', 'isRegistered': False}
vendor.update(vendor)
vendor = {'name': 'vendor2', 'contact': '9633017633', 'email': 'jbiju94@gmail.com', 'isRegistered': False}
vendor.update(vendor)
vendor = {'name': 'vendor3', 'contact': '9633017633', 'email': 'jbiju94@gmail.com', 'isRegistered': False}
vendor.update(vendor)

items.append("pen drive")
items.append("trophies")
items.append("bean bags")
items.append("pens")

vendor_item = {'name': 'pen drive', 'vendor': 'vendor1', 'price': 200}
vendor_has.update(vendor_item)
vendor_item = {'name': 'trophies', 'vendor': 'vendor1', 'price': 500}
vendor_has.update(vendor_item)
vendor_item = {'name': 'pens', 'vendor': 'vendor2', 'price': 100}
vendor_has.update(vendor_item)
vendor_item = {'name': 'bean bags', 'vendor': 'vendor3', 'price': 900}
vendor_has.update(vendor_item)
vendor_item = {'name': 'trophies', 'vendor': 'vendor3', 'price': 800}
vendor_has.update(vendor_item)


# Trigger new event
@app.route('/registerNewVendor', methods=['POST'])
def register_new_vendor():
    name = request.form['name']
    for _vendor in vendors:
        if _vendor[name] is name:
            _vendor['isRegistered'] = True
            name = _vendor['name']
            break

    return "Vendor " + name + "has been added."


# Getting response for a specific payload
@app.route('/findVendorWithItem', methods=['POST'])
def find_vendors_with_items():
    item_name = request.form['name']
    _vendors = dict()
    for _vendor_item in vendor_has:
        if _vendor_item['name'] is item_name:
            _vendors.update(_vendor_item['vendor'])
    return json.dumps(_vendors)


@app.route('/isVendorRegistered', methods=['POST'])
def is_vendor_registered():
    name = request.form['name']
    for _vendor in vendors:
        if _vendor[name] is name:
            if _vendor['isRegistered']:
                return True
    return False


@app.route('/addRequest', methods=['POST'])
def add_request():
    vendor = request.form['name']
    item = request.form['item']
    qty = request.form['qty']
    user = request.form['user']

    req = {'vendor': vendor, 'qty': qty, 'item': item, 'user': user, 'id': uuid.uuid4(), 'status': 'ISSUED'}
    requests.update(req)

    return json.dumps(req)


@app.route('/getRequest', methods=['POST'])
def add_request():
    id = request.form['id']
    for rq in requests:
        if rq['id'] is id:
            return json.dumps(rq)


@app.route('/updateResponse', methods=['POST'])
def update_request():
    id = request.get['req']
    val = requests.get['val']
    for rq in requests:
        if rq['id'] is id:
            if val is 'YES':
                rq['status'] = "VENDOR_CONFIRMED"
            else:
                rq['status'] = "VENDOR_DECLINED"
        break
    return "Confirmation Noted"


if __name__ == '__main__':
    app.run(port=6000)
