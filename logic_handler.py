import requests
import json
from email_handler import EmailHandler
import selectVendor




class LogicHandler:
    API_SERVER_URL = "http://localhost:5003/"

    def __init__(self, email):
        email = json.loads(email)
        self.__to = email['To']
        self.__from = email['From']
        self.__subject = email['Subject']
        self.__body = email['Body']
        self.__attachments = email['Attachment']

    """
    # Extract Required Data from response
    """
    @staticmethod
    def __extract_data(data):
        data = json.loads(data)
        if data['status']['code'] == 200:
            source = data['result']['source']
            source = 'domain'
            query = data['result']['resolvedQuery']
            action = data['result']['action']
            response = data['result']['fulfillment']['speech']
            if source is 'agent':
                intent = data['result']['contexts']['name']
                parameters = data['result']['contexts']['parameters']
            else:
                intent = "GENERAL_CASE"
                parameters = ""
            intent_data = {"source": source,
                           "query": query,
                           "action": action,
                           "response": response,
                           "intent": intent,
                           "parameters": parameters
                           }
            return intent_data
        else:
            LogicHandler.__throw_error()

    """
    # request for intent for text
    """

    def get_intent_data(self):
        r = requests.post(LogicHandler.API_SERVER_URL + "getIntent", data={'text': self.__body})
        print(r.status_code, r.reason)
        if r.status_code == 200:
            return self.__extract_data(r.content)
        else:
            LogicHandler.__throw_error()

    @staticmethod
    def __throw_error():
        return "Error"

    def trigger_work_flow(self):
        intent_data = self.get_intent_data()
        eH = EmailHandler()
        if intent_data['source'] is 'domain':
            eH.send_mail1( self.__to, self.__from, self.__subject, intent_data['response'], self.__attachments)
        else:
            if intent_data['intent'] is "proc_item_details":
                item = intent_data['parameters'].procurement_item
                qty  = intent_data['parameters'].number
                r = requests.post("http://localhost:6002/findVendorWithItem", data={'name': item})
                vendors = json.loads(r.content)
                # find best vendor
		vendor = selectVendor.vs(['vendor1','vendor2'])
                #vendor = vendors[0]
                # check if registered vendor
                r = requests.post("http://localhost:6002/addRequest", data={'name': vendor, 'item': item, 'qty': qty, 'user': self.__from})
                request_resp = json.loads(r.content)
                r = requests.post("http://localhost:6002/isVendorRegistered", data={'name': vendor})
                if r.content:
                    # trigger mail
                    eH.send_mail1(self.__to, self.__from, self.__subject, "Do you sell " + item + "?", self.__attachments, "ASK_PROMT ", request_resp['id'])
                else:
                    # ask him to register
                    eH.send_mail1(self.__to, self.__from, self.__subject, "Kindly Register for vendor", self.__attachments, "REGISTER_PROMT", request_resp['id'])
            
        
        return "Status: 200"
